# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

setup(
    name="pyconvenience",
    version="0.5.1",
    packages=find_packages(),
    install_requires=[
        'pyyaml>=5.3.1,<6.0.0'
    ],
    author="Tom Brouwer",
    author_email="tombrouwer@outlook.com",
    description="Convenience scripts to do repetitive actions in python",
    license="Apache-2.0",
)
